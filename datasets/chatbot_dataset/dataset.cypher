// neo4j graph database
// using Cypher Query Language

// Please delete all existing graphs using the following line
// MATCH (n) DETACH DELETE (n)
// please observe all nodes using the following line
// MATCH (n) RETURN (n)

// configuration
:config initialNodeDisplay: 3000;

//...................... remove previous data .........................................................................
MATCH (n) DETACH DELETE n;


// ................... User Highise ........................
MERGE (E1: User {Highise_level: 'High'})
MERGE (E2: User {Highise_level: 'Intermediate'})
MERGE (E3: User {Highise_level: 'Low'})


//........................ components......................
MERGE (C1: Component {
    component_en: 'Fan',
    component_it: 'Fan'
    })

MERGE (C2: Component {
    component_en: 'Panel',
    component_it: 'Mascherina'
    })

MERGE (C3: Component {
    component_en: 'Pen',
    component_it: 'Penna'
    })

MERGE (C4: Component {
    component_en: 'Book',
    component_it: 'Prenotare'
    })

MERGE (C5: Component {
    component_en: 'Parts',
    component_it: 'Parti'
    })

MERGE (C6: Component {
    component_en: 'Covers',
    component_it: 'Coperture'
    })

MERGE (C7: Component {
    component_en: 'Accessories',
    component_it: 'Accessori'
    })

MERGE (C8: Component {
    component_en: 'Leakages',
    component_it: 'Fughe'
    })

MERGE (C9: Component {
    component_en: 'Label',
    component_it: 'Etichetta'
    })

MERGE (C10: Component {
    component_en: 'Microphone',
    component_it: 'Microfono'
    })

MERGE (C11: Component {
    component_en: 'Packaging',
    component_it: 'Imballo'
    })


//......................checklist Steps............................

MERGE (T1: Step { step: '1', step_id: '1', time: 120, type_always: ['High']})
MERGE (T2: Step {step: '2', step_id: '1.1', time: 60, type_always: ['Intermediate']})
MERGE (T3: Step {step: '3', step_id: '1.1.1', time: 20, type_always: ['Low'], type_random: ['Intermediate']})
MERGE (T4: Step {step: '4', step_id: '1.1.2', time: 20, type_always: ['Low'], type_random: ['Intermediate']})
MERGE (T5: Step {step: '5', step_id: '1.1.3', time: 20, type_always: ['Low'], type_random: ['Intermediate']})
MERGE (T6: Step {step: '6', step_id: '1.2', time: 40, type_always: ['Intermediate']})
MERGE (T7: Step {step: '7', step_id: '1.2.1', time: 10, type_always: ['Low'], type_random: ['Intermediate']})
MERGE (T8: Step {step: '8', step_id: '1.2.2', time: 10, type_always: ['Low'], type_random: ['Intermediate']})
MERGE (T9: Step {step: '9', step_id: '1.2.3', time: 10, type_always: ['Low'], type_random: ['Intermediate']})
MERGE (T10: Step {step: '10', step_id: '1.2.4', time: 10, type_always: ['Low'], type_random: ['Intermediate']})



// ........................ Step - Objectives and Actions ..............................................

// ............................Step_id- 1................
MERGE (T: Step {step: '1', step_id: '1', time: 120, type_always: ['High']})

MERGE (O: Object {
    objective_en: 'objective1',
    objective_it: 'objective1'
    })
MERGE (A: Action {
    action_en: 'action1',
    action_it: 'action1'
    })
// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 2................................
MERGE (T: Step {step: '2'})

MERGE (O: Object {
    objective_en: 'objective2',
    objective_it: 'objective2'
    })
MERGE (A: Action {
    action_en: 'action2',
    action_it: 'action2'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 3................................
MERGE (T: Step {step: '3'})

MERGE (O: Object {
    objective_en: 'objective3',
    objective_it: 'objective3'
    })
MERGE (A: Action {
    action_en: 'action3',
    action_it: 'action3'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 4................................
MERGE (T: Step {step: '4'})

MERGE (O: Object {
    objective_en: 'objective4',
    objective_it: 'objective4'
    })
MERGE (A: Action {
    action_en: 'action4',
    action_it: 'action4'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 5................................
MERGE (T: Step {step: '5'})

MERGE (O: Object {
    objective_en: 'objective5',
    objective_it: 'objective5'
    })
MERGE (A: Action {
    action_en: 'action5',
    action_it: "action5"
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 6................................
MERGE (T: Step {step: '6'})

MERGE (O: Object {
    objective_en: 'objective6',
    objective_it: 'objective6'
    })
MERGE (A: Action {
    action_en: 'action6',
    action_it: "action6"
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 7................................
MERGE (T: Step {step: '7'})

MERGE (O: Object {
    objective_en: 'objective7',
    objective_it: 'objective7'
    })
MERGE (A: Action {
    action_en: 'action7',
    action_it: 'action7'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 8................................
MERGE (T: Step {step: '8'})

MERGE (O: Object {
    objective_en: 'objective8',
    objective_it: 'objective8'
    })
MERGE (A: Action {
    action_en: 'action8',
    action_it: 'action8'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 9................................
MERGE (T: Step {step: '9'})

MERGE (O: Object {
    objective_en: 'objective9',
    objective_it: 'objective9'
    })
MERGE (A: Action {
    action_en: 'action9',
    action_it: 'action9'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


// ....................Step_id- 10................................
MERGE (T: Step {step: '10'})

MERGE (O: Object {
    objective_en: 'objective10',
    objective_it: 'objective10'
    })
MERGE (A: Action {
    action_en: 'action10',
    action_it: 'action10'
    })

// definition of the relationships,
CREATE
(T)-[:CONTAINS] -> (O),
(T)-[:CONTAINS] -> (A);


//..................... user profiles, product family and product profiles(with product status)..............................................
// ................. user profiles ...........................
// MATCH (n) DETACH DELETE (n)

CREATE (UP1: UserProfile {
    user_id: '0012345abcd',
    user_Highise_level: 'High',
    user_language: 'en'
    })

CREATE (UP2: UserProfile {
    user_id: '002345abcd',
    user_Highise_level: 'Low',
    user_language: 'en'
    })

CREATE (UP3: UserProfile {
    user_id: '00345abcd',
    user_Highise_level: 'Intermediate',
    user_language: 'en'
    })

CREATE (UP4: UserProfile {
    user_id: '0045abcd',
    user_Highise_level: 'Intermediate',
    user_language: 'it'
    })

CREATE (UP5: UserProfile {
    user_id: '005abcd',
    user_Highise_level: 'Low',
    user_language: 'nl'
    });


//.............................product family........................
CREATE (PF1: ProductFamily {
    name:'Pen'
    })

//.............................product profiles........................
CREATE (PI1: ProductProfile {
    product_identifier:'3454671234',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI2: ProductProfile {
    product_identifier:'3454671235',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI3: ProductProfile {
    product_identifier:'3454671236',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI4: ProductProfile {
    product_identifier:'3454671237',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: ['1'],
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI5: ProductProfile {
    product_identifier:'3454671238',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })


CREATE (PI6: ProductProfile {
    product_identifier:'3454671239',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI7: ProductProfile {
    product_identifier:'3454671244',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI8: ProductProfile {
    product_identifier:'3454671245',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })

CREATE (PI9: ProductProfile {
    product_identifier:'3454671246',
    product_identifier_type:'DKU',
    checklist_id: '1',
    checklist_is_active: False,
    active_checklist_step: '1',
    checklist_is_completed: False,
    date_checked_for_recommendation: 'not_given'
    })


// ................. product status ...........................
CREATE (ST1: Status {
    name: 'registered'
    })
CREATE (ST2: Status {
    name: 'produced'
    })
CREATE (ST3: Status {
    name: 'launched'
    })
CREATE (ST4: Status {
    name: 'planned'
    })

MERGE (UP1: UserProfile {
    user_id: '00012345abcd',
    user_Highise_level: 'High',
    user_language: 'en'
    })

CREATE(UP1)-[:HAS_SCANNED]->(PI1),
(PI1)-[:BELONGS_TO] -> (PF1), (PI1)-[:HAS_STATUS] -> (ST2),
(PI2)-[:BELONGS_TO] -> (PF1), (PI2)-[:HAS_STATUS] -> (ST2),
(PI3)-[:BELONGS_TO] -> (PF1), (PI3)-[:HAS_STATUS] -> (ST2),
(PI4)-[:BELONGS_TO] -> (PF1), (PI4)-[:HAS_STATUS] -> (ST2),
(PI5)-[:BELONGS_TO] -> (PF1), (PI5)-[:HAS_STATUS] -> (ST2),
(PI6)-[:BELONGS_TO] -> (PF1), (PI6)-[:HAS_STATUS] -> (ST2),
(PI7)-[:BELONGS_TO] -> (PF1), (PI7)-[:HAS_STATUS] -> (ST2),
(PI8)-[:BELONGS_TO] -> (PF1), (PI8)-[:HAS_STATUS] -> (ST2),
(PI9)-[:BELONGS_TO] -> (PF1), (PI9)-[:HAS_STATUS] -> (ST2);