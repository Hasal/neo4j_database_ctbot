from neo4j import GraphDatabase
import sys
import re

host_name = sys.argv[1]
user_name = sys.argv[2]
password = sys.argv[3]

uri = f"bolt://{host_name}:7687"


driver = GraphDatabase.driver(uri, auth=(user_name, password))


def clean_db(tx):
    tx.run("MATCH (n) DETACH DELETE (n)")


# Based on this:
# https://stackoverflow.com/questions/39217929/split-string-on-commas-but-ignore-commas-with-in-single-quotes-and-create-a-dict
def load_from_file(tx):
    file = open("datasets/checklist_demo/en_checklist_database.cypher", "r")
    # Slits by semicolon unless the semicolon is in a single quite
    messages = re.split(";(?=(?:[^']*\'[^']*\')*[^']*$)", file.read())

    for message in messages:
        print("###################")
        print(message)
        if message:
            tx.run(message)

    file.close()


with driver.session() as session:
    session.write_transaction(clean_db)
    session.write_transaction(load_from_file)


driver.close()
