# Neo4j_database_ctbot

### An example database with docker

0. Deploy on server in neo4j folder
```
    git clone git@gitlab.ips.biba.uni-bremen.de:wel/neo4j-server.git neo4j 
    git checkout staging-neo4j-testdatabase
```

```
2. Run the container
```
    docker-compose up -d
```

This works for wsl2 or linux based docker engine, for windows hyper-v based docker engine check neo4j documents.
Note that this may take several seconds to execute the initial database imports and establishing the connection

3. Then go to http://localhost:7474/browser and enter the username and password
4. username = neo4j, password = neo4jtestdb
5. you can then access the neo4j database through the neo4j browser
6. use "MATCH (n) RETURN n" to retrieve all graph nodes (empty on startup)
7. copy the contents of the database.cypher file to the query terminal 
8. execute the query to add the demo data

##### following steps can be used to work with neo4j cypher shell 
1. use **"docker ps"** to get the docker container ID (Cont_ID) 
2. Run the cypher shell using the command,  
        docker exec -it cont_ID cypher-shell

3. username = neo4j, password = neo4jtestdb 
4. use following command to access the graph database database  
     " match (n) return (n);"

##### following step can be used to improve the graph visualization
5. you can use the **style.grass** file to change the graph database style(drag and drop into neo4j Browser)